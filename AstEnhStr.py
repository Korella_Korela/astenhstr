"""
    We've 2 ways: destroy items or destroy blesses
    So, there is a strategy chooser (* -*)=/
    """

class AstEnhStr():
    """Class to choose enhans strategy"""
    def __init__(self, chance, num, crys, c_p, i_p, e_p, b_p):
        self.chance = chance
        self.num = num
        self.crys = crys
        self.cr_pr = c_p
        self.it_pr = i_p
        self.en_pr = e_p
        self.bl_pr = b_p

    def default_way(self, amount):
        i=0
        enh_waste = 0
        while i < self.num:
            if i <= 4:
                enh_waste += self.en_pr*amount
            else:
                enh_waste += self.en_pr*(amount*(self.chance**(i-4))) # -3? -4?
            i+=1
        
        items_waste = amount*self.it_pr
        crys_bonus = (amount-1)*self.cr_pr*self.crys
        result = enh_waste + items_waste - crys_bonus
        print('\nDefauls way:')
        print('\tScroll waste: ', enh_waste, ' --> ', int(enh_waste/1000/1000), 'kk', ' --> ', int(enh_waste/1000/1000/1000), 'kkk')
        print('\tItems waste: ', items_waste, ' --> ', int(items_waste/1000/1000), 'kk', ' --> ', int(items_waste/1000/1000/1000), 'kkk')
        print('\tCrystals bonus: ', crys_bonus, ' --> ', int(crys_bonus/1000/1000), 'kk', ' --> ', int(crys_bonus/1000/1000/1000), 'kkk')
        return result

    def bless_way(self, amount):
        i=0
        enh_waste = 0
        bless_waste = 0
        while i < self.num:
            if i <= 4:
                enh_waste += self.en_pr*amount
            else:
                bless_waste += self.bl_pr*(amount*(self.chance**(i-4))) # -3? -4?
            i+=1
        result = self.it_pr + enh_waste + bless_waste
        print('\nBless way:')
        print('\tScroll waste: ', enh_waste, ' --> ', int(enh_waste/1000/1000), 'kk', ' --> ', int(enh_waste/1000/1000/1000), 'kkk')
        print('\tBless waste: ', bless_waste, ' --> ', int(bless_waste/1000/1000), 'kk', ' --> ', int(bless_waste/1000/1000/1000), 'kkk')
        print('\tItems waste: ', self.it_pr, ' --> ', int(self.it_pr/1000/1000), 'kk', ' --> ', int(self.it_pr/1000/1000/1000), 'kkk')
        return result

    def choose_strategy(self):
        if self.num > 3:
            amount = 1/(self.chance**(self.num-3))
            way_d = int(self.default_way(amount))
            way_b = int(self.bless_way(amount))
            print('\nResults:')
            print('\tDefault scroll way costs: ', way_d, ' --> ', int(way_d/1000/1000), 'kk', ' --> ', int(way_d/1000/1000/1000), 'kkk')
            print('\tBless   scroll way costs: ', way_b, ' --> ', int(way_b/1000/1000), 'kk', ' --> ', int(way_b/1000/1000/1000), 'kkk\n')
        else:
            print('\tDefault is better:', (self.it_pr + self.en_pr*self.num))


def main():
    chance = float(input('\nInput enhancement chance (Ex: 0.52): '))
    num = int(input('\nInput enhancement number (Ex: 8): '))
    crys = int(input('\nInput amount of crystalls (Ex: 1234): '))
    c_p = float(input('\nInput crystal price (Ex: 8000): '))
    i_p = float(input('\nInput item price (Ex: 213475975): '))
    e_p = float(input('\nInput scroll price (Ex: 5): '))
    b_p = float(input('\nInput bless price (Ex: 55): '))
    enh = AstEnhStr(chance, num, crys, c_p, i_p, e_p, b_p)
    enh.choose_strategy()

if __name__ == "__main__":
    main()
